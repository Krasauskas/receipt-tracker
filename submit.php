<?php

$server = '127.0.0.1';
$user = 'receipt';
$password = 'stupidPassword';
$dbname = 'receipt_db';

if (isset($_POST['sum'])) {
	$sum = $_POST['sum'];
}
if (isset($_POST['photo'])) {
	$photo = $_POST['photo'];
	$photoName = md5_file($_POST['photo']);
}
if ($sum != null) {
	try {
		$conn = new PDO("mysql:host=$server;dbname=$dbname", $user, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare("INSERT INTO Receipts (sum, photoName)
			VALUES (:sum, :photo");
		$stmt->bindParam(':sum', $sum);
		$stmt->bindParam(':photoName', $photoName);

		$stmt->execute();
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
	$conn = null;
}